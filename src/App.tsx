import React from 'react';
import './App.css';
import { Button, TextField, MenuItem,FormControl,  InputLabel, Select, Box, List, IconButton, ListItemText, ListItem } from '@mui/material'
import DeleteIcon from '@mui/icons-material/Delete';

const pageStyles = {
  color: "#232129",
  padding: 20,
  fontFamily: "-apple-system, Roboto, sans-serif, serif",
  backgroundColor: 'rgb(240 240 240)',
  height: '100%'
}
const headingStyles = {
  marginTop: 0,
  marginBottom: 4,
  maxWidth: 320,
  color: 'black',
  fontWeight: '600',
}

function App() {
  const [banks, setBanks] = React.useState<{name: string, id: number}[]>([])
  const [cashBacks, setCashBacks] = React.useState<{name: string, id: number, bank_id: number, cash_back: string}[]>([])
  const [newNameBank, setNewNameBank] = React.useState('')
  const [newCashBack, setNewCashBack] = React.useState<{name: string, bank_id: number, cash_back: string}>({name: '', bank_id: 0, cash_back: ''})

  const addBank = () => {
    if (newNameBank) {
      const nextId = (banks[banks.length-1]?.id ?? 0) + 1;
      setBanks([...banks, {name: newNameBank, id: nextId}])
      setNewNameBank('')
      localStorage.setItem('cash-back', JSON.stringify({banks: [...banks, {name: newNameBank, id: nextId}], cashBacks: cashBacks }));
    }
  }

  const deleteBank = (idBank: number) => {
    setBanks([...banks.filter((bank) => bank.id !== idBank)])
    localStorage.setItem('cash-back', JSON.stringify({banks: [...banks.filter((bank) => bank.id !== idBank)], cashBacks: cashBacks }));
  }

  const addCashBack = () => {
    if (newCashBack.bank_id && newCashBack.name && newCashBack.cash_back) {
      const nextId = (cashBacks[cashBacks.length - 1]?.id ?? 0) + 1;
      setCashBacks([...cashBacks, {...newCashBack, id: nextId}])
      localStorage.setItem('cash-back', JSON.stringify({banks: banks , cash_back: [...cashBacks, {...newCashBack, id: nextId}]}));
    }
  }

  const deleteCashBack = (idCashBack: number) => {
    setCashBacks([...cashBacks.filter((cashBack)=> cashBack.id !== idCashBack)])
    localStorage.setItem('cash-back', JSON.stringify({banks: banks , cash_back: [...cashBacks.filter((cashBack)=> cashBack.id !== idCashBack)]}));
  }

  React.useEffect(()=> {
    const data = localStorage.getItem('cash-back');
    if (data) {
      const cashBackInfo = JSON.parse(data);
      setBanks((cashBackInfo.banks ?? []).filter((bank:{name: string, id: number})=>  bank.name));
      setCashBacks((cashBackInfo.cash_back ?? []).filter((cashBack:{name: string, id: number, bank_id : number})=>  cashBack.name && cashBack.bank_id));
    }
  },[])
  
  return (
    <div className="App" style={pageStyles}>
      <header>
        <h1 style={headingStyles}>Cash back</h1>
      </header>

      <Box>
        {banks.map((bank)=> 
          <ListItem
            sx={{
              flexDirection: 'column',
              alignItems: 'baseline',
              backgroundColor: 'white',
              marginBottom: 2,
              borderRadius: 2
            }}
          >
            <ListItemText/>
            <Box sx={{width: '100%', display: 'flex', justifyContent: 'space-between'}}>
              {bank.name}
              <IconButton edge="end" aria-label="delete" onClick={() => deleteBank(bank.id)}>
                <DeleteIcon />
              </IconButton>      
            </Box>
            <List sx={{ mt: 1, width: '100%' }}>
              {cashBacks.filter( cashBack => cashBack.bank_id === bank.id).map((cashBack) => 
                <ListItem
                  sx={{backgroundColor: 'aliceblue', marginBottom: 2, borderRadius: 2}}
                  secondaryAction={
                    <IconButton edge="end" aria-label="delete" onClick={() => deleteCashBack(cashBack.id)}>
                      <DeleteIcon />
                    </IconButton>
                  }
                >
                  <ListItemText
                    primary={`${cashBack.name} - ${cashBack.cash_back}`}
                  />
                </ListItem>,
              )}
            </List>
          </ListItem>
        )}
      </Box>

      <Box 
        component="section" 
        sx={{
          p: 2,
          marginTop: 2,
          display: 'flex',
          backgroundColor: 'white',
          flexDirection: 'column',
          borderRadius: 2
        }}
      >
        <FormControl sx={{ m: 1, minWidth: 120 }} size="small">
          <InputLabel id="demo-select-bank-label">банк</InputLabel>
          <Select
            labelId="demo-select-bank-label"
            id="demo-select-bank"
            value={newCashBack.bank_id}
            displayEmpty={true}
            label="банк"
            onChange={(e)=> setNewCashBack( {...newCashBack, bank_id: Number(e.target.value)})}
          >
            {banks.map((bank) => 
              <MenuItem value={bank.id}>
                {bank.name}
              </MenuItem>
            )}
          </Select>
          <TextField
            id="new-category"
            label="категория"
            size="small"
            variant="outlined"
            value={newCashBack.name}
            sx={{marginTop: 0.5}}
            onChange={(e) =>setNewCashBack({...newCashBack, name: e.target.value})} 
          />
          <TextField
            id="new-%"
            label="%"
            size="small"
            variant="outlined"
            value={newCashBack.cash_back}
            sx={{marginTop: 0.5}}
            onChange={(e) =>setNewCashBack({...newCashBack, cash_back: e.target.value})} 
          />
          <Button
            variant="contained"
            sx={{marginTop: 0.5}}
            onClick={addCashBack}
          >
            Добавить cash back
          </Button>
        </FormControl>
      </Box>

      <Box
        component="section"
        sx={{ p: 2, marginTop: 2,display: 'flex', flexDirection: 'column', backgroundColor: 'white', borderRadius: 2 }}
      >
        <TextField
          id="new-bank"
          label="банк"
          size="small"
          variant="outlined"
          value={newNameBank}
          onChange={(e) =>setNewNameBank(e.target.value)}
        />
        <Button
          variant="contained"
          sx={{marginTop: 0.5}}
          onClick={addBank}
        >
          Добавить банк
        </Button>
      </Box>
    </div>
  )
}

export default App;
